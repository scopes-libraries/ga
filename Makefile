.RECIPEPREFIX+=*

SCOPES_INSTALL_DIRECTORY=/usr/local/lib/scopes

.PHONY: check install test

check: init.sc
* scopes init.sc

test:
* scopes testing/test.sc

install:
* rm -f $(SCOPES_INSTALL_DIRECTORY)/ga
* ln -s $(shell pwd) $(SCOPES_INSTALL_DIRECTORY)/ga

