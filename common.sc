using import spicetools

using import .types

spice rotor (dim type)
    let args =
        sc_argument_list_map_new
            dim as i32 // 2 + 1
            inline (i)
                i * 2
    spice-quote
        (MultiVector.from-grades type dim args) . VersorType

run-stage;

let rotor

inline scal (element-type)
    MultiVector.from-grades element-type 0 0

inline scalof (ty val)
    (scal ty) val

inline scalof (args...)
    """"Generates a scalar value for any dimension of `type` initialized by `val`. If `type` is omitted, it is inferred.
    let count = (va-countof args...)
    static-match count
    case 1
        let val = args...
        scalof (typeof val) val
    case 2
        let ty val = args...
        scalof ty val
    default
        static-error "scalof requires exactly one or two arguments"
        

inline vec (dim type)
    MultiVector.from-grades type dim 1

spice vecof (args...)
    """"Genrates a vector of `type` initialized using varargs or keyword arguments.
        If an integer is the first argument, instead of type, the dimension is explicitly set.
    spice-match args...
    case (dim : integer, T : type, values...)
        spice-quote
            (vec dim T) values...
    case (T : type, values...)
        let dim = ('argcount values...)
        spice-quote
            (vec dim T) values...
    default
        error
            "Wrong arguments for vecof; supply a dimension and a type or a type and the number of arguments, the dimension requires"


inline bivec (dim type)
    MultiVector.from-grades type dim 2

inline bivecof (dim type ...) =
    """"Generates a bivector for dimension `dim` of `type` initialized using varargs or keyword arguments.
    (bivec dim type) ...
inline rotorof (dim type ...) =
    """"Generates a rotor for dimension `dim` of `type` initialized using varargs or keyword arguments.
    (rotor dim type) ...


inline pscal (dim type)
    MultiVector.from-grades type dim dim

inline pscalof (dim type val)
    """"Generates a pseudoscalar for dimension `dim` of `type` initialized by `val`
    (pscal dim type) val



locals;

