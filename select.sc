using import utils.va
using import utils.functional

using import .blades

fn geometric? (a b) true

inline contraction? (a b)
    not
        or
            ('grade a) > ('grade b)
            ('grade (a * b)) != ('grade b) - ('grade a)

fn left-contraction? (a b)
    contraction? a b

fn right-contraction? (a b)
    contraction? b a

fn outer? (a b)
    (a as i32 & b as i32) == 0

fn scalar? (a b)
    a * b == (Blade.from-name "e")

fn dot? (a b)
    let val =
        or
            contraction? a b
            contraction? b a
    val

fn inner? (a b)
    and
        dot? a b
        not (scalar? a b)

inline compile-blade-function (fun)
    typify fun Blade Blade

let geometric? left-contraction? right-contraction? outer? scalar? dot? inner? =
    va-map compile-blade-function
        geometric?
        left-contraction?
        right-contraction?
        outer?
        scalar?
        dot?
        inner?

locals;

