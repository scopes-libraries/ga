This aims to be a very efficient library for geometric algebra.

## Dependencies
* [scopes](http://scopes.rocks), version 0.16
* [utils](https://gitlab.com/scopes-libraries/utils), version 0.1

## Examples

Look in the testing directory to see some examples of usage.

The file [example.sc](testing/example.sc) also contains a few explanations.

## Usage

### General Multi Vector Types

The main geometric algebra type is the multi vector. It's a generalization of a vector.
The abstract multivector type is called `MultiVector` in this library.

You can create specific multivector types like this:
`MultiVector element-type blades...`
This will create a multivector type, whose elements are stored as `element-type`.
The specified `blades...` can each be generated using `e!` followed by the basis vectors of the blade.

For example `e1` can be specified using `e! 1` and the 3D pseudo scalar `e123` can be specified using `e! 1 2 3`.
The order of the blade arguments doesn't matter, it will always represent one of the types.

Here are a few examples for creating a type:
```scopes
# 3D vector
MultiVector f32 (e! 1) (e! 2) (e! 3)

# 3D rotor
MultiVector f32 (e!) (e! 1 2) (e! 1 3) (e! 2 3)
```

Also here, if you switch the order of the arguments, they will be sorted automatically for consistnecy reasons.

You can bind these types to names using `let` like this:

```scopes
let my-multi-vector-type =
    Multivector f32 (e! 1) (e! 2) (e! 3)
```

Calling this type like a function creates a new value of the type:

```scopes
my-multi-vector-type 1 2 3
```

When you don't want to create your multi vector type before initializing, there's a simpler way, which automatically creates the type when initializing the value:

```
# Create a value of a 3D rotor using e1, e2 and e-12 as basis blades.
# Notice when the blade numbrs have two digits, they are seperated by `-`
MultiVector! f32
    e = 1.0
    e-12 = 0.0
    e-1-12 = 0.0
    e-2-12 = 0.0
```

### Simple Multi Vector Types

There are some predefined types, which simplify creating common multi vectors:

* `scal T` => a scalar type (only grade 0; `e`) of type `T`
* `vec N T` => a vector type of dimension `N` (only grade 1; `e1`, `e2`, ..., `eN`) with elements of type `T`
* `bivec N T` => a bivector type of dimension `N` (only grade 2; `e12`, `e13`, ..., `e(N-1)N`) with elements of type `T`
* `rotor N T` => a rotor type of dimension `N` (even subalgebra; all `e-x-...` for x from 1 to `N`, where the count of basis vectors per blade is even) with elements of type `T`
* `pscal N T` => a pseudo scalar type of dimension `N` (`e12...N`) of type `T`

These types can be used as constructors on their own, but it's also possible to use some simplified constructors instead:

* `scalof T val` => `(scal T) val`
* `vecof [N] T elements...` => `(vec N T) elements...`; see `N` is optional and will be known by the number of specified elements
* `bivecof N T elements...` => `(bivec N T) elements...`
* `rotorof N T elements...` => `(rotor N T) elements...`
* `pscalof N T val` => `(pscal N T) val`

### Versors

It's tracked at compile time, which types are versors.

The multi vector type will automatically converted to a versor, when every value of this type will be a versor.
All values, which can be written as products of vectors are vectors.
So scalars, vectors, pseudo vectors and pseudo scalars are always versors.

This allows you to implment custom behavior for versor types and to catch bugs, when your operation requires the type to be a versor, but you created a non-versor multi vector instead.

If your multi vector constructor doesn't create a versor type, you can create it manually using the specific `Versor` constructor instead.
It works just like `MultiVector`, but always creates a versor type. Only use it, when you can ensure a value to be a versor and don't know another way to create the type (for example when loading object information stored in a file from a previous session, where you knew the data were versors).

### Casting

Casting of multivectors is possible using `as`.
You can either cast to a specific type or to the type `Versor`, which just ensures the type to be recognized as a versor.

The casting rules will probably have to be updated.

### Math

Some basic mathematical functions work for multi vectors.

Addition and subtraction only work, when the type of both arguments is the same:

```scopes
let
    a = (vecof f32 1 2 3)
    b = (vecof f32 3 2 1)
    c = (MultiVector! f32 (e1 = 1))

a + b # works
a - b # works
a + c # doesn't work
a + c as (vec 3 f32) # works
```

You see, when casting explicitly it works nevertheless.
Addition doesn't preserve a type to be a versor, except all elements of this type are versors, like in this case for vectors.

Products on the other hand work with any kinds of multivector.

There is the geometric product, the outer product and several kinds of inner products.

Only the geometric product preserves the vesor type.

Only for the geometric product and the outer product there is a infix version.

All geometric products also have a method version, which supports the output type as a third argument.
This will make the multiplication more efficient if you want to cast afterwards anyway.

```scopes
# geometric product
a * b
'geometric a b

# outer product
a ^ b
'outer a b

# scalar product
'scalar a b

# inner left contraction
'left a b

# inner right contraction
'right a b

# fat dot product
'dot a b

# hestenes inner product
'inner a b
```

