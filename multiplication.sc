using import spicetools

using import utils.va

using import .blades
using import .types
import .select

spice static-vector-size (count)
    count := count as usize
    loop (i = 2)
        if (i < count)
            i * 2
        else
            break i

fn multiply (a ac b bc select?)
    let val =
        fold (val = 0) for i in (range ac)
            blade := a @ i
            let m =
                i32 (floor (log2 (blade as f32)))
            max m val
    let val =
        fold (val = val) for i in (range bc)
            blade := b @ i
            let m =
                i32 (floor (log2 (blade as f32)))
            max m val
    let max-blades = (2 ** (val + 1))

    let blade-array =
        malloc-array bool max-blades

    for i in (range max-blades)
        blade-array @ i = false

    for ia in (range ac)
        ba := a @ ia
        for ib in (range bc)
            bb := b @ ib
            if (select? ba bb)
                blade-array @ ((ba * bb) as usize) = true

    local blades : BladeList
    let track =
        malloc-array i32 max-blades

    for i in (range max-blades)
        if (blade-array @ i)
            track @ i = (countof blades) as i32
            'append blades (i as Blade)
    _
        deref blades
        blade-array
        track

@@ memoize
fn make-multiply (aT bT versor? select output-blades)
    let a-blades a-count b-blades b-count =
        ('@ aT 'Blades) as (mutable pointer Blade)
        ('@ aT 'Count) as usize
        ('@ bT 'Blades) as (mutable pointer Blade)
        ('@ bT 'Count) as usize

    let output-blades blade-array blade-proxy =
        static-if (none? output-blades)
            multiply a-blades a-count b-blades b-count select
        else
            _ output-blades
                'to-blade-array output-blades

    let output-type =
        MultiVector.from-blades versor?
            '@ aT 'ElementType
            output-blades

    let count =
        countof output-blades

    if (count < 0x10)

        inline multiply-blades (a b)
            let sums =
                alloca-array Value count

            for i in (range count)
                sums @ i = 0

            for ai in (range a-count)
                a-blade := a-blades @ ai
                for bi in (range b-count)
                    b-blade := b-blades @ bi
                    let out-blade = (a-blade * b-blade)
                    if (blade-array @ (out-blade as usize))
                        let i =
                            blade-proxy @ (out-blade as usize)
                        sums @ i =
                            spice-quote
                                +
                                    [(deref (sums @ i))]
                                    *
                                        aT.ElementType
                                            spice-unquote
                                                ? ('flip? a-blade b-blade) -1 1
                                        extractelement a ai
                                        extractelement b bi

            sc_argument_list_map_new (i32 count)
                inline (i)
                    sums @ i

        spice-quote
            fn "multiply" (a b)
                output-type [(multiply-blades a b)]
    else
        spice-quote
            fn "multiply" (a b)
                local result : output-type
                for ai in (range a-count)
                    a-blade := a-blades @ ai
                    for bi in (range b-count)
                        b-blade := b-blades @ bi
                        let out-blade = (a-blade * b-blade)
                        if (blade-array @ (out-blade as usize))
                            let i =
                                blade-proxy @ (out-blade as usize)
                            (bitcast (reftoptr result) (mutable pointer aT.ElementType)) @ i +=
                                *
                                    aT.ElementType
                                        ? ('flip? a-blade b-blade) -1 1
                                    extractelement a ai
                                    extractelement b bi
                deref result

inline generate-multiply (select?)
    spice "generate-multiply" (a b args...)
        let aT bT =
            va-map 'typeof a b

        assert (('@ bT 'ElementType) == ('@ bT 'ElementType))
            "Element types of multi vectors have to be the same for multiplication"

        let versor? select? =
            static-if (none? select?)
                _
                    ('@ aT 'Versor?) as bool and ('@ bT 'Versor?) as bool
                    select.geometric?
            else
                _
                    false
                    select?

        let multiply =
            spice-match args...
            case ()
                make-multiply aT bT versor? select? none
            case (blades as BladeList,)
                make-multiply aT bT versor? select? (view blades)
            case (versor as bool,)
                make-multiply aT bT versor select? none
            case (multi-vector as type,)
                assert
                    or
                        ('@ multi-vector 'ElementType) == void
                        ('@ multi-vector 'ElementType) == ('@ aT 'ElementType)

                versor? := ('@ multi-vector 'Versor?) as bool
                blades := ('@ multi-vector 'Blades) as (mutable pointer Blade)
                local blade-list : BladeList
                for i in (range (('@ multi-vector 'Count) as usize))
                    'append blade-list
                        blades @ i

                make-multiply aT bT versor? select? (view blade-list)
            default
                error "Invalid arguments to multivector multiplication"
        spice-quote
            multiply a b

let multiply scalar left right dot inner outer =
    generate-multiply;
    generate-multiply select.scalar?
    generate-multiply select.left-contraction?
    generate-multiply select.right-contraction?
    generate-multiply select.dot?
    generate-multiply select.inner?
    generate-multiply select.outer?

typedef+ MultiVector
    let
        generate-multiplication = generate-multiply

    let
        geometric = multiply
        scalar = scalar
        left = left
        right = right
        dot = dot
        inner = inner
        outer = outer

    let
        __* =
            box-pointer
                spice-binary-op-macro
                    inline (aT bT)
                        if (aT < MultiVector and bT < MultiVector)
                            `multiply
                        elseif (aT < MultiVector and ('@ aT 'ElementType) == bT)
                            spice-quote
                                inline (a b)
                                    bitcast
                                        (storagecast a) * (vectorof aT.ElementType (va-fill (static-vector-size aT.Count) b))
                                        aT
                        else `()

        __r* =
            box-pointer
                spice-binary-op-macro
                    inline (aT bT)
                        if (bT < MultiVector and ('@ bT 'ElementType) == aT)
                            spice-quote
                                inline (a b)
                                    bitcast
                                        (vectorof bT.ElementType (va-fill (static-vector-size bT.Count) a)) * (storagecast b)
                                        bT
                        else `()

        __/ =
            box-pointer
                spice-binary-op-macro
                    inline (aT bT)
                        if (aT < MultiVector and ('@ aT 'ElementType) == bT)
                            spice-quote
                                inline (a b)
                                    bitcast
                                        (storagecast a) / (vectorof aT.ElementType (va-fill (static-vector-size aT.Count) b))
                                        aT
                        else `()
        __^ =
            box-pointer
                spice-binary-op-macro
                    inline (aT bT)
                        if (aT < MultiVector and bT < MultiVector)
                            `outer
                        else `()

run-stage;

typedef+ Versor
    inline norm2 (self)
        (scalar self ('reverse self)) . e

    inline norm (self)
        sqrt
            abs
                'norm2 self

    inline normalize (self)
        self / ('norm self)

    inline inverse (self)
        ('reverse self) / ('norm2 self)

    let
        __r/ =
            box-pointer
                spice-binary-op-macro
                    inline (aT bT)
                        if (bT < Versor and ('@ bT 'ElementType) == aT)
                            spice-quote
                                inline (a b)
                                    a * ('inverse b)
                        else `()

    fn exp (self) # TODO for Blades?
        define result
            let len =
                'norm self
            let T = (typeof self)
            let max-grade =
                MultiVector.max-grade T

            inline grade-exp (grade)
                let value-type =
                    MultiVector.sub-grade-type T grade
                static-if (value-type.Count > 0)
                    let value =
                        self as value-type
                    let norm =
                        'norm value
                    let scalar-factor value-factor =
                        static-if (grade % 2 == 0)
                            (va-map-functions cosh sinh) norm
                        else
                            (va-map-functions cos sin) norm

                    let scalar-type =
                        MultiVector T.ElementType (e!)
                    let scalar =
                        scalar-type scalar-factor

                    let value =
                        value * norm * value-factor

                    let sum-type =
                        MultiVector.sum-type scalar-type value-type

                    +
                        scalar as sum-type
                        value as sum-type

            static-match T.Count
            case 0
                MultiVector! T.ElementType
                    e =
                        1 as T.ElementType
            case 1
                grade-exp max-grade
            default
                let args... =
                    va-map grade-exp
                        va-range (max-grade + 1)
                static-match (va-countof args...)
                case 0
                    MultiVector! T.ElementType
                        e =
                            1 as T.ElementType
                case 1
                    args...
                default
                    * args...
        result as Versor

    fn rotate (self vec)
        'geometric self (vec * ('inverse self)) (typeof vec)

