using import ..init

# three basis vectors represented by a single value

# basis vector x
let x =
    MultiVector! f32
        e1 = 1.0

# basis vector y
let y =
    MultiVector! f32
        e2 = 1.0

# basis vector z
let z =
    MultiVector! f32
        e3 = 1.0

# simpler way to create values of basic vector types
local pos =
    # sorted by blade number: e1, e2, e3
    vecof f32 1 2 3

# create a simpler name for your default vector type
let vec3 =
    vec 3 f32

# initialize this vector type directly
local vel =
    vec3 1 2 3

# addition works too
pos += vel

# represent directional orientation as rotor
local dir =
    # remember 3D-Rotors have 4 components
      sorted by blade grade, then by blade numbers: e e12 e13 e23
      cast to `Versor` necessary, since non-versor rotors would cause wrong rotations
    (rotorof 3 f32 1 0 0 0) as Versor

# set the rotation to a rotation along the xy-axis
dir =
    # `x * y` returns a rotor, which is a versor, but only has components e and e12
      `as (rotor 3 f32)` converts to the correct representation, but loses the versor type
      `as Versor` recreates the versor type, so the result is correct again
      still seems a bit difficult, I'm hoping for suggestions
    (x * y) as (rotor 3 f32) as Versor

# represent rotational velocity as bivector
local rot =
    bivecof 3 f32 0 0 0

# set the non-rotated front vector to the unit y vector
local front =
    y as vec3

# increase rotational velocity along the plane between the front vector and the z vector
rot +=
    # The symbol `^` represents the outer product:
      (front ^ y) as (bivec 3 f32)
      Instead a more efficient form in the next row is used,
      which doesn't require casting after the multiplication:
    'outer front y (bivec 3 f32)

# apply the rotational velocity to the directional orientation
dir *=
    'exp rot

# recalculate the front vector afterwards
front =
    # use the sandwich product
      (dir * front * dir) as vec3
      the cast in the end is necessary
      because the result will contain a trivector component
      instead of the cast we'll specify the output of the second product again
    'rotate dir front
