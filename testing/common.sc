using import testing

using import ..types
using import ..common
import ..multiplication

let vec =
    vecof f32 1 2 3
let vec0 =
    vecof f32 1 2 3

let bivec =
    bivecof 3 f32 1 2 3

let rotor =
    rotorof 3 f32 0 1 2 3

print vec vec0 bivec rotor

test
    vec == vec0

using import ..blades

print
    MultiVector f32 (e! 1) (e! 2) (e! 3)
    typeof vec

print
    scal f32
    scalof 1
    scalof f32 1

let R =
    rotorof 3 f32 1 0 0 0

let V =
    vecof f32 1 2 3

print "RVR"
    'geometric (R * V) ('reverse R) (typeof V)

