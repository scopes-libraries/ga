using import testing

using import utils
using import utils.va

using import ..blades
using import ..types
import ..multiplication
import ..select

let vec3 =
    MultiVector f32
        e! 1
        e! 2
        e! 3

let quat =
    Versor f32
        e!;
        e! 1 2
        e! 1 3
        e! 2 3

let a b =
    vec3 1 2 3
    vec3 3 2 1

print
    a * 5.0
    5.0 * a

let c =
    a * b

test
    (typeof c) == quat

va-map print "geometric:"
    a * b
    a * c
    c * a
    'geometric a c vec3

va-map print "outer:"
    a ^ b
    a ^ c
    c ^ a
    'outer a c true
    'outer a c vec3

va-map print "scalar:"
    'scalar a b
    'scalar a c
    'scalar c a

va-map print "left contraction:"
    'left a b
    'left a c
    'left c a

va-map print "right contraction:"
    'right a b
    'right a c
    'right c a

va-map print "dot:"
    'dot a b
    'dot a c
    'dot c a

va-map print "inner:"
    'inner a b
    'inner a c
    'inner c a

va-map print "norms:"
    'norm a
    'norm b
    'norm c
    'norm
        MultiVector! f32
            e12 = 1
            e13 = 2
            e23 = 3

va-map print "normalized:"
    'normalize a
    'normalize b
    'normalize c

let b1 b2 =
    *
        vec3 1 0 0
        vec3 0 1 0
    'outer
        vec3 1 0 0
        vec3 0 1 0

print "exp:"
    b1
    'exp b1
    b2
    'exp b2


let a b =
    vec3 1 2 3
    vec3 3 2 1

let c d =
    a * b
    b * a

test
    (typeof a) < Versor

test
    (typeof (a + b)) < Versor

test
    (typeof c) < Versor

test
    not ((typeof (c + d)) < Versor)

test
    and
        a.e1 == 1
        a.e2 == 2
        a.e3 == 3

test-compiler-error
    a.e4

