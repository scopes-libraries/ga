using import testing

using import ..blades
using import ..types

let e e1 e2 e3 e12 e13 e23 =
    e!;
    e! 1
    e! 2
    e! 3
    e! 1 2
    e! 1 3
    e! 2 3

let vec3 =
    MultiVector f32 e1 e2 e3

let other-vec3 =
    MultiVector f32 e1 e2 e3

test
    vec3 == other-vec3
    "Memoization of multivectors failed"

let other-vec3 =
    MultiVector f32 e3 e2 e1

test
    vec3 == other-vec3
    "Memoization or sorting of multivectors failed"

inline test-versor (arg)
    test (arg < Versor)
        "Automatic conversion to versor type failed"

inline test-not-versor (arg)
    test (not (arg < Versor))
        "Automatic conversion to versor type not allowed for this type"

let bivec3 =
    MultiVector f32 e12 e13 e23

let vec4 =
    MultiVector f32 e1 e2 e3

let bivec4 =
    MultiVector f32 e12 e13 e23
        e! 1 4
        e! 2 4
        e! 3 4

let complex =
    MultiVector f32 e e12

let quat =
    MultiVector f32 e e12 e13 e23

let zero =
    MultiVector f32

let scalar =
    MultiVector f32 e

let blade1 =
    MultiVector f32 e1

let simple =
    MultiVector f32 e e1

va-map test-versor
    vec3
    vec4
    bivec3
    complex
    zero
    scalar
    blade1

va-map test-not-versor
    bivec4
    quat
    simple

let vec =
    MultiVector! f32
        e1 = 0.2
        e23 = 0.5
        e4 = 0.11
        e123 = 5.5

test
    (bivec3 1 2 3) as quat == (quat 0 1 2 3)

test
    (quat 0 1 2 3) as bivec3 == (bivec3 1 2 3)

let some-quat =
    quat 1 0 0 0

test
    not
        (typeof some-quat) < Versor

let some-quat-versor =
    some-quat as Versor

test
    (typeof some-quat-versor) < Versor


test
    (quat 0 1 2 3) as (MultiVector i32 e12 e13 e23) ==
        MultiVector! i32
            e12 = 1
            e13 = 2
            e23 = 3

va-map print
    'reverse (vec3 1 2 3)
    'reverse (bivec3 1 2 3)
    'reverse (quat -1 1 2 3)

    'involute (vec3 1 2 3)
    'involute (bivec3 1 2 3)
    'involute (quat -1 1 2 3)

    'conjugate (vec3 1 2 3)
    'conjugate (bivec3 1 2 3)
    'conjugate (quat -1 1 2 3)

