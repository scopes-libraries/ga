using import testing

using import utils
using import utils.va

using import ..blades

inline local-test (arg)
    test arg "Basis error"

test
    Blade == Blade

let e =
    e!;

let e1 =
    e! 1

let e235 =
    e! 2 3 5

let e-10 =
    e! 10

let e-1-23 =
    e! 1 23

va-map local-test
    e1 as i32 == 2
    e235 as i32 == 44
    e as i32 == 0

    ('grade e1) == 1
    ('grade e235) == 3
    ('grade e) == 0
    
    not ('flip? e1 e1)
    'flip? e235 e235
    not ('flip? e1 e235)

    e1 < e235
    e1 > e
    e1 < e-10

let a b c =
    'vectors e235

va-map local-test
    a == 2
    b == 3
    c == 5

va-map local-test
    "e" == ('name e)
    "e1" == ('name e1)
    "e235" == ('name e235)
    "e-10" == ('name e-10)
    "e-1-23" == ('name e-1-23)

inline test-name (name)
    test (('name (Blade.from-name name)) == name)

va-map test-name "e" "e1" "e235" "e-10" "e-1-23"

