using import spicetools
using import Array
using import utils.va

using import .blades

typedef MultiVector < immutable
typedef Versor < MultiVector

spice static-arrayof-blade (value...)
    let count = ('argcount value...)
    let values = (malloc-array Blade count)

    for i in (range count)
        values @ i = ('getarg value... i) as Blade

    values

fn to-argument-list (something)
    something := something as BladeList
    sc_argument_list_map_new (i32 (countof something))
        inline (i)
            something @ i

spice static-blades-name (blades...)
    let blades-name =
        fold (result = "") for blade in ('args blades...)
            .. result " " ('name (blade as Blade))

spice static-vector-size (count)
    count := count as usize
    loop (i = 2)
        if (i < count)
            i * 2
        else
            break i

fn vector-size (count)
    loop (i = 2)
        if (i < count)
            i * 2
        else
            break i

run-stage;

inline make-typename (versor? element-type count blades...)

    let blades-name =
        static-blades-name blades...

    let name =
        .. "<"
            static-if versor?
                "Versor"
            else
                "MultiVector"
            " "
            tostring element-type
            blades-name
            ">"
    let blades-array =
        static-arrayof-blade blades...

    let storage-type =
        static-if (count == 0) (tuple)
        else
            vector element-type
                static-vector-size count

    typedef
        do name
        \ < (do (static-if versor? Versor) (else MultiVector))
        \ : storage-type
        let Versor? = versor?
        let ElementType = element-type
        let Count = count
        let Blades = blades-array

@@ memo
inline make-typenames (secure-versor? element-type count blades...)
    let VersorType =
        make-typename true element-type count blades...
    let NonVersorType =
        static-if secure-versor? VersorType
        else
            make-typename false element-type count blades...

    typedef+ VersorType
        let VersorType NonVersorType
    typedef+ NonVersorType
        let VersorType NonVersorType

    _ VersorType NonVersorType


inline make-typename (secure-versor? versor? element-type count blades...)
    let VersorType NonVersorType =
        make-typenames secure-versor? element-type count blades...
    static-if versor? VersorType
    else NonVersorType

fn multivector-type (versor? element-type blades)

    let secure-versor? =
        'versor? blades

    let count =
        countof blades

    let blades =
        to-argument-list blades

    spice-quote
        make-typename secure-versor? (versor? | secure-versor?)
            [(element-type as type)]
            count
            blades

unlet make-typename

spice make-multivector (versor? element-type blades...)
    local blades : BladeList
    for val in ('args blades...)
        'append blades (val as Blade)
    'sort blades
    multivector-type versor? element-type (deref blades)

spice MultiVector! (element-type blades...)
    local blades : BladeList
    local result : (Array Value 0x40) # TODO: fix for no parameter
    for blade in ('args blades...)
        let k v =
            'dekey blade
        'append blades (Blade.from-name (k as Symbol as string))
        'append result v
    local new-blades : BladeList
    'sort-and-remove blades new-blades
    let result =
        sc_argument_list_new
            i32 (countof result)
            reftoptr (result @ 0)
    let vector-type =
        multivector-type false element-type (deref new-blades)
    spice-quote
        vector-type result

spice dimension-multivector (element-type dim grades...)
    local blades : BladeList
    for val in (range (1 << dim as i32))
        let blade =
            (val << 1) as Blade
        for grade in ('args grades...)
            if (('grade blade) == grade as i32)
                'append blades blade
                break;
    local new-blades : BladeList
    'sort-and-remove blades new-blades
    multivector-type false element-type (deref new-blades)

inline multivector-value (self args...)
    let result =
        static-if (va-empty? args...)
            (storageof self);
        else
            static-assert
                ==
                    va-countof args...
                    self.Count
                "Multi vector types can only be created with either one value for each element or no arguments"
            static-match self.Count
            case 0
                tupleof;
            default
                let size = (static-vector-size self.Count)
                vectorof self.ElementType ((va-join args...) (va-fill (size - self.Count) 0))
    bitcast
        result
        self

typedef+ MultiVector
    let from-blades = multivector-type

    let from-grades = dimension-multivector

    spice __typecall (ty args...)
        match ty
        case (or MultiVector Versor)
            spice-match args...
            case (element-type : type, args...)
                spice-quote
                    make-multivector (ty == Versor) element-type args...
            default
                error "Wrong arguments for element"
        default
            spice-quote
                multivector-value ty args...

    fn __repr (self)
        tostring (storagecast self)

    inline make-sum (f)
        box-pointer
            spice-binary-op-macro
                inline (aT bT)
                    if (('@ aT 'NonVersorType) == ('@ bT 'NonVersorType))
                        spice-quote
                            fn (a b)
                                bitcast
                                    f (storagecast a) (storagecast b)
                                    aT.NonVersorType
                    else `()

    let __+ __- =
        make-sum +
        make-sum -


    let __neg =
        box-pointer
            inline (self)
                let T =
                    typeof self
                let storage =
                    storagecast self
                let count =
                    i32 (countof storage)
                let zeros... = (va-fill count (0 as T.ElementType))
                bitcast (- (vectorof T.ElementType zeros...) storage) T
    @@ spice-binary-op-macro
    fn __== (lhsT rhsT)
        if (lhsT == rhsT)
            spice-quote
                inline (lhs rhs)
                    all?
                        (storagecast lhs) == (storagecast rhs)
        else `()

    let __as =
        spice-cast-macro
            @@ memoize
            fn make-as (vT T)
                if (T == Versor)
                    return
                        spice-quote
                            inline (val)
                                bitcast val vT.VersorType
                let in-blades output-blades =
                    ('@ vT 'Blades) as (mutable pointer Blade)
                    ('@ T 'Blades) as (mutable pointer Blade)

                let count = (('@ T 'Count) as usize)

                inline caster (val)
                    let sums =
                        alloca-array Value count

                    for i in (range count)
                        sums @ i = `(0 as vT.ElementType)

                    for ai in (range (('@ vT 'Count) as usize))
                        in-blade := in-blades @ ai
                        for i in (range count)
                            blade := output-blades @ i
                            if (blade == in-blade)
                                sums @ i =
                                    spice-quote
                                        (extractelement val ai) as vT.ElementType
                                break;

                    sc_argument_list_map_new (i32 count)
                        inline (i)
                            sums @ i

                spice-quote
                    fn "multiply" (val)
                        T [(caster val)]

spice static-Symbol->string (sym)
    sym as Symbol as string

spice static@-blade (value i)
    (value as (mutable pointer Blade)) @ (i as i32)

spice static-string== (a b)
    a as string == b as string

spice mapped-vector (T fun)
    T := T as type
    let blades =
        ('@ T 'Blades) as (mutable pointer Blade)
    let fun =
        fun as (pointer (function bool Blade))
    let count =
        ('@ T 'Count) as usize
    let args =
        sc_argument_list_map_new (vector-size count)
            inline (i)
                if (i < count)
                    blade := blades @ i
                    ? (fun blade) 1 -1
                else
                    0
    spice-quote
        vectorof T.ElementType args

run-stage;

inline blade-index (T attr)
    va-lfold none
        inline (key i value)
            let blade =
                static@-blade T.Blades i
            static-if (attr == blade) i
            else value
        va-range (i32 T.Count)

typedef+ MultiVector
    inline __getattr (val attr)
        let name = (static-Symbol->string attr)
        let name =
            static-if (static-string== name "x") "e1"
            elseif (static-string== name "y") "e2"
            elseif (static-string== name "z") "e3"
            elseif (static-string== name "w") "e4"
            else name

        let attr =
            Blade.static-from-name name

        let T =
            typeof val
        let result =
            blade-index T attr
        static-if (none? result)
            static-error
                .. "Blade \"" name "\" does not exist in type " (tostring T)
        else
            extractelement val result

    inline __@ (self index)
        let T = (typeof index)
        static-if (T == Blade)
            let result =
                blade-index T index
            extractelement self result
        elseif (T < integer)
            extractelement self index
        else
            static-error "Extracting is only possible for blades and integers"


    spice sum-type (aT bT)
        let aT bT =
            aT as type
            bT as type
        let aE bE =
            ('@ aT 'ElementType) as type
            ('@ bT 'ElementType) as type
        assert (aE == bE)
            "Common sum type can only be generated, if the element type is the same"

        let a ac b bc =
            ('@ aT 'Blades) as (mutable pointer Blade)
            ('@ aT 'Count) as usize
            ('@ bT 'Blades) as (mutable pointer Blade)
            ('@ bT 'Count) as usize

        local blades : BladeList

        for i in (range ac)
            'append blades (a @ i)
        for i in (range bc)
            'append blades (b @ i)

        local result-blades : BladeList

        'sort-and-remove blades result-blades

        multivector-type false aE result-blades

    spice sub-grade-type (T grade)
        T := T as type
        let versor? E blades count =
            ('@ T 'Versor?) as bool
            ('@ T 'ElementType) as type
            ('@ T 'Blades) as (mutable pointer Blade)
            ('@ T 'Count) as usize

        local result-blades : BladeList

        for i in (range count)
            if (('grade (blades @ i)) == grade)
                'append result-blades (blades @ i)
        multivector-type versor? E result-blades

    spice max-grade (T)
        T := T as type
        let blades count =
            ('@ T 'Blades) as (mutable pointer Blade)
            ('@ T 'Count) as usize
        fold (max-grade = -1) for i in (range count)
            let grade =
                'grade (blades @ i)
            max grade max-grade

    inline generate-mapping (test?)
        inline "mapping" (self)
            let T =
                typeof self
            static-if (T.Count == 0)
                self
            else
                bitcast
                    *
                        storagecast self
                        mapped-vector T test?
                    T

    let reverse involute conjugate =
        va-map generate-mapping Blade.reverse? Blade.involute? Blade.conjugate?

do
    let
        MultiVector
        Versor
        MultiVector!
    locals;
