using import Array

using import utils.va
using import utils.functional
using import utils.math

fn char->int (c)
    assert (0x30 <= c and c <= 0x39) "Char not a number"
    c - 0x30

fn string->int (s start end)
    loop (result i = 0 start)
        if (i == end)
            return result end
        let c =
            s @ i
        if (c == 0x2d)
            return result i
        else
            let n =
                char->int c
            _ (result * 10 + n) (i + 1)

typedef Blade < immutable : u32
    fn from-name (name)
        assert (name @ 0 == 0x65)
            """"Blade names start with letter "e"
        let len =
            i32 (countof name)
        bitcast
            if (name @ 1 == 0x2d)
                loop (result i = 0 2)
                    if (i < len)
                        let num i =
                            string->int name i len
                        repeat (result | 1 << num) (i + 1)
                    break result
            else
                loop (result i = 0 1)
                    if (i < len)
                        let num =
                            char->int (name @ i)
                        repeat (result | 1 << num) (i + 1)
                    break result
            this-type

    spice static-from-name (name)
        from-name (name as string)
    
    let __== = integer.__==

    let __as =
        spice-cast-macro
            fn "Blade-as" (vT T)
                spice-quote
                    inline (expr)
                        (bitcast expr [('storageof vT)]) as T

    let __ras =
        spice-cast-macro
            fn "Blade-ras" (vT T)
                spice-quote
                    inline (expr)
                        bitcast (expr as [('storageof vT)]) T

    fn grade (a)
        let a =
            deref
                a as i32
        loop (a c = a 0)
            if (a > 0)
                _ (a >> 1) 
                    if ((a & 1) != 0)
                        c + 1
                    else c
            else
                break c
    
    fn vector? (self i)
        (self as i32) & (1 << i) != 0

    fn flip? (a b)
        let a b =
            (deref a) as i32
            (deref b) as i32
        loop (a b c = a b 0)
            if ((a >> 1) > 0)
                _ (a >> 1) b (c + (grade ((a >> 1) & b)))
            else
                break
                    (c & 1) != 0

    inline make-grade-order (f)
        box-pointer
            simple-binary-op
                fn (a b)
                    let ga gb =
                        va-map grade a b
                    if (ga == gb)
                        f (a as u32) (b as u32)
                    else
                        f ga gb

    let __< __> __<= __>= =
        va-map make-grade-order
            _ <
            _ >
            _ <=
            _ >=

    unlet make-grade-order

    let __* = integer.__^

run-stage;


fn blade-name (blade)
    let blade =
        blade as i32
    let separator =
        if (blade >= 1 << 10)
            "-"
        else
            ""
    loop (i result = 0 "e")
        if ((1 << i) <= blade)
            _
                i + 1
                ..
                    result
                    if (((1 << i) & blade) != 0)
                        .. separator (tostring i)
                    else
                        ""
        else
            break result

spice blade-name (blade)
    if ('constant? blade)
        let result =
            blade-name
                blade as Blade
        `result
    else
        spice-quote
            blade-name blade

spice make-blade (type vecs...)
    bitcast
        fold (result = 0) for vec in ('args vecs...)
            (1 << vec as i32) | result
        Blade

typedef+ Blade
    fn reverse? (a)
        let grade =
            'grade a
        even? (grade * (grade - 1) // 2)

    fn involute? (a)
        even? ('grade a)

    fn conjugate? (a)
        let grade =
            'grade a
        even? (grade * (grade + 1) // 2)

    inline make-function (fun)
        compile
            typify fun this-type

    let reverse? involute? conjugate? =
        va-map make-function reverse? involute? conjugate?
    
    unlet make-function

    spice vectors (blade)
        let blade =
            blade as this-type as i32
        
        local vectors : (Array Value)

        loop (i = 0)
            if ((1 << i) <= blade)
                i + 1
            else
                break i
        
            if (((1 << i) & blade) != 0)
                'append vectors i
            i + 1
        
        sc_argument_list_new
            (countof vectors) as i32
            reftoptr (@ vectors 0)
    let
        name = blade-name
        __repr = blade-name
        __typecall = make-blade

unlet blade-name make-blade

inline e! (vecs...)
    Blade vecs...

let BladeList =
    GrowingArray Blade

run-stage;

typedef+ BladeList
    fn versor? (blades)
        let count =
            i32
                countof blades
        or
            ## Simpe check for single blades
            count == 1
            ## Simple check for vectors
            fold (result = true) for blade in (blades as BladeList)
                if (('grade (blade as Blade)) != 1)
                    break false
                result
            ## Complicated check for complicated versor types
            loop (i result bitcount = 0 0 0)
                let finished zero-count zero-pos one-count one-pos =
                    fold (finished zero-count zero-pos one-count one-pos = true 0 0 0 0) for n blade in (enumerate (blades as BladeList))
                        let blade =
                            blade as Blade
                        _
                            ((1 << i) > blade as i32) & finished
                            if ('vector? blade i)
                                _
                                    zero-count
                                    zero-pos
                                    one-count + 1
                                    if (one-count == 0)
                                        n
                                    else
                                        one-pos
                            else
                                _
                                    zero-count + 1
                                    if (zero-count == 0)
                                        n
                                    else
                                        zero-pos
                                    one-count
                                    one-pos
                if finished
                    break
                        bitcount == count
                _
                    i + 1
                    if (zero-count == 1)
                        let zero-bit =
                            1 << zero-pos
                        if (zero-bit & result == 0)
                            _
                                zero-bit | result
                                bitcount + 1
                        elseif (one-count == 1)
                            let one-bit =
                                1 << one-pos
                            if (one-bit & result == 0)
                                _
                                    one-bit | result
                                    bitcount + 1
                            else
                                break
                                    bitcount == count
                        else
                            break
                                bitcount == count
                    elseif (one-count == 1)
                        let one-bit =
                            1 << one-pos
                        if (one-bit & result == 0)
                            _
                                one-bit | result
                                bitcount + 1
                        else
                            break
                                bitcount == count
                    else
                        _ result bitcount

    fn sort-and-remove (old-blades blades)
        'sort old-blades
        local last-blade : Blade
        local first = true
        for blade in old-blades
            if ((deref first) or blade != last-blade)
                first = false
                last-blade = blade
                'append blades blade

    fn to-blade-array (self)
        let val =
            fold (val = 0) for i blade in (enumerate self)
                let m =
                    i32 (floor (log2 (blade as f32)))
                max m val
        let max-blades = (2 ** (val + 1))

        let blade-array =
            malloc-array bool max-blades
        
        for i in (range max-blades)
            blade-array @ i = false
        
        for ba in self
            blade-array @ (ba as usize) = true

        let track =
            malloc-array i32 max-blades

        local count = 0

        for i in (range max-blades)
            if (blade-array @ i)
                track @ i = count as i32
                count += 1
        _
            blade-array
            track


do
    let Blade BladeList e!
    locals;

